package main

import (
	"net/http"
	"html/template"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"time"
	"log"
	"strings"
)


type Movie struct {
	Name string
	Storyline string
	Ratings float32
	Year time.Time
	Duration int
	Thumbnail string
	Genre string
	Poster string
	Id int
}



var (
	tmplDetail = template.Must(template.New("detail").ParseFiles("base.html","detail.html", "index.html"))
	tmplList = template.Must(template.New("list").ParseFiles("base.html","list.html", "index.html"))
	db, _ = sql.Open("sqlite3", "db.sqlite3")


)


func loadMovie(title string) (*Movie) {
	var name string
	var storyline string
	var ratings float32
	var year time.Time
	var duration int
	var thumbnail string
	var genre string
	var poster string
	var id int 

	q, err := db.Query("select name, storyline,genre,poster, ratings, year, duration,thumbnail,id from movies_movies where name = '" + title + "' ")
	defer q.Close()
	if err != nil {
		return nil
	}
	for q.Next() {
		q.Scan(&name, &storyline, &genre,&poster, &ratings,&year, &duration, &thumbnail,&id)
	}
	return &Movie{Name: name, Storyline:storyline, Ratings:ratings, Year:year, Duration:duration, Thumbnail:thumbnail, Genre:genre, Poster:poster, Id:id}
}

func loadList()([]Movie){
	
	q,err := db.Query("SELECT name,storyline,genre,poster,ratings,year,duration,thumbnail from movies_movies")
	defer q.Close()
	if err != nil{
		return nil
	}
	var retVal []Movie
	
	for q.Next(){
		movie := Movie{}
		err = q.Scan(&movie.Name, &movie.Storyline, &movie.Genre, &movie.Poster, &movie.Ratings, &movie.Year, &movie.Duration, &movie.Thumbnail )
		
		if err != nil{
			log.Fatal("Error scannin rows", err)
		}

		retVal = append(retVal, movie)

	}

	return retVal

}

func search(query string)([]Movie){
	q,err := db.Query("SELECT name,storyline,genre,poster,ratings,year,duration,thumbnail FROM movies_movies where name LIKE '%" + query + "%' OR genre LIKE '%" + query + "%' OR year LIKE '%" + query + "%' ")
	defer q.Close()
	if err != nil{
		return nil
	}
	var retVal []Movie
	
	for q.Next(){
		movie := Movie{}
		err = q.Scan(&movie.Name, &movie.Storyline, &movie.Genre, &movie.Poster, &movie.Ratings, &movie.Year, &movie.Duration, &movie.Thumbnail )
		
		if err != nil{
			log.Fatal("Error scannin rows", err)
		}

		retVal = append(retVal, movie)

	}

	return retVal

}

func sort(sortby string)([]Movie){
	q,err := db.Query("SELECT name,storyline,genre,poster,ratings,year,duration,thumbnail FROM movies_movies ORDER BY " + sortby + " ")
	defer q.Close()
	if err != nil{
		return nil
	}
	var retVal []Movie
	
	for q.Next(){
		movie := Movie{}
		err = q.Scan(&movie.Name, &movie.Storyline, &movie.Genre, &movie.Poster, &movie.Ratings, &movie.Year, &movie.Duration, &movie.Thumbnail )
		
		if err != nil{
			log.Fatal("Error scannin rows", err)
		}

		retVal = append(retVal, movie)

	}

	return retVal

}


func detailView(w http.ResponseWriter, r *http.Request) {

	title := r.URL.Path[len("/detail/"):]
	if title == ""{
		title = "Interstellar"
	}
	p := loadMovie(title)
	
	tmplDetail.ExecuteTemplate(w, "base", p)
}

func listView(w http.ResponseWriter, r *http.Request) {
	// title := r.URL.Path[len("/list/"):]
	p := loadList()
	getreq := r.URL.Query()["q"]
	getsort := r.URL.Query()["sort_by"]
	query := strings.Join(getreq,"")
	sortby := strings.Join(getsort," ")

	if sortby == "ratings" || sortby == "year"{
		sortby = sortby + " DESC"
	}

	if query != "" {
		p = search(query)
	}
	if query == "" && sortby !=""{
		p = sort(sortby)
	}

	tmplList.ExecuteTemplate(w, "base", p)
	
}


func main() {
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	http.Handle("/movies/", http.StripPrefix("/movies/", http.FileServer(http.Dir("movies"))))
	http.HandleFunc("/detail/", detailView)
	http.HandleFunc("/list/", listView)
	http.ListenAndServe(":8080", nil)

	
}
